package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;

public class Dealer {

  private DeckOfCards deckOfCards;

  /**
   * Constructor for the Dealer class.
   * @param deckOfCards The deck of cards to be used by the dealer.
   */
  public Dealer(DeckOfCards deckOfCards) {
    this.deckOfCards = deckOfCards;
  }

  /**
   * Method for giving out a random card from the deck, using the Math.random method.
   * This method takes in an int "n", where the input specifies how many cards to give out.
   * The random number can not be more than the amount of cards left in the deck.
   * The dealt card is returned from the method and removed from the deck.
   * @param n Number of cards to be dealt.
   * @return A list of random cards from the deck.
   * @throws IllegalArgumentException if there are not enough cards in the deck.
   */
  public List<PlayingCard> dealHand(int n) {
    if (n > deckOfCards.getDeck().size()) {
      throw new IllegalArgumentException("Not enough cards in the deck");
    }
    ArrayList<PlayingCard> hand = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      int random = (int) (Math.random() * deckOfCards.getDeck().size());
      hand.add(deckOfCards.getDeck().get(random));
      deckOfCards.getDeck().remove(random);
    }
    deckOfCards.resetDeck(); // Resets the deck after dealing the hand.
    return hand;
  }

}

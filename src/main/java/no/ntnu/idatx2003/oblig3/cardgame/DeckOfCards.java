package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;

public class DeckOfCards {



  protected ArrayList<PlayingCard> deck = new ArrayList<>();

  /**
   * Fills a deck with 52 cards. Adding the cards to the arraylist deck.
   */
  protected void deckBuilder() {
    for (char suit : new char[] {'S', 'H', 'D', 'C'}) {
      for (int face = 1; face <= 13; face++) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }

  /**
   * Constructor for the class DeckOfCards.
   * Calls the deckBuilder method to fill the deck with 52 cards.
   */
  public DeckOfCards() {
    deckBuilder();
  }

  /**
   * Method for getting the deck.
   * @return The deck of cards.
   */
  protected ArrayList<PlayingCard> getDeck() {
    return deck;
  }

  /**
   * Resets the deck by calling the deckBuilder method.
   */
  public void resetDeck() {
    deck.clear();
    deckBuilder();
  }


}

package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;

public class Hand {

  private List<PlayingCard> cards;

  /**
   * Constructor for the Hand class.
   * Creates a new hand with no cards.
   * The cards in the hand are stored in an ArrayList.
   */
  public Hand() {
    this.cards = new ArrayList<>();
  }

  /**
   * Deals a hand of cards from the dealer.
   * @param dealer The dealer to deal the hand from.
   * @param numCards The number of cards to deal.
   */
  public void dealHand(Dealer dealer, int numCards) {
    cards = dealer.dealHand(numCards);
  }

  /**
   * Checks if the hand is a flush. A flush is a hand where all the cards have the same suit.
   * @return true if the hand is a flush, false otherwise.
   */
  public boolean isFlush() {
  boolean isFlush = false;

  if (!cards.isEmpty()) {
    char suit = cards.get(0).getSuit();
    isFlush = cards.stream().allMatch(card -> card.getSuit() == suit);
  }

  return isFlush;
  }

  /**
   * Adds a card to the hand.
   * @param card The card to add to the hand.
   */
  public void addCard(PlayingCard card) {
    cards.add(card);
  }

  /**
   * Returns the cards in the hand.
   * @return The cards in the hand.
   */
  public List<PlayingCard> getCards() {
    return cards;
  }

  /**
   * Calculates the total value of all the cards in the hand.
   * @return The total value of all the cards.
   */
  public int getTotalValue() {
    return cards.stream().mapToInt(PlayingCard::getFace).sum();
  }

}

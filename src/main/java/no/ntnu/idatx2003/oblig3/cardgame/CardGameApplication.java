package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.List;
import java.util.stream.Collectors;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The main class for a simple card game application using JavaFX.
 */
public class CardGameApplication extends Application{

  private Hand hand;
  private TextArea handDisplay;
  private Label flushLabel;
  private Label totalValueLabel;
  private Label heartsLabel;
  private Label queenOfSpadesLabel;
  private Button dealHandButton;
  private Dealer dealer; // Initiates a instance of the Dealer class, here in the CardGameApplication class.

  /**
   * The main entry point for all JavaFX applications.
   * The start method is called after the init method has returned,
   * and after the system is ready for the application to begin running.
   *
   * @param primaryStage the primary stage for this application, onto which
   *                     the application scene can be set.
   */
  @Override
  public void start(Stage primaryStage) {
    // Creates a menu bar and a menu option to exit the application.
    MenuBar menuBar = new MenuBar();
    Menu fileMenu = new Menu("File");
    MenuItem exitItem = new MenuItem("Exit");
    exitItem.setOnAction(e -> System.exit(0)); // Exit application when clicked.
    fileMenu.getItems().add(exitItem);
    menuBar.getMenus().add(fileMenu);

    // Opprett instansen av Dealer
    DeckOfCards deck = new DeckOfCards(); // Antar at du har en slik konstruktør
    dealer = new Dealer(deck);


    handDisplay = new TextArea();
    flushLabel = new Label();
    hand = new Hand();
    totalValueLabel = new Label();
    heartsLabel = new Label();
    queenOfSpadesLabel = new Label();

    // Button for dealing a hand of cards
    dealHandButton = new Button("Deal hand");
    dealHandButton.setOnAction(e -> dealHand());


    // VBox to organize the button and text area
    VBox centerLayout = new VBox(10, dealHandButton, handDisplay, flushLabel, totalValueLabel,
      heartsLabel, queenOfSpadesLabel);
    centerLayout.setSpacing(10); // Space between components


    // BorderPane as the root layout
    BorderPane root = new BorderPane();
    root.setTop(menuBar);
    root.setCenter(centerLayout);

    // Creates a scene and sets the stage.
    Scene scene = new Scene(root, 800, 600);
    primaryStage.setTitle("Card Game Application");
    primaryStage.setScene(scene);
    primaryStage.show();

  }

  /**
   * Deals a hand of cards and updates the display.
   */
  private void dealHand() {
    hand.dealHand(dealer, 5);
    updateHandDisplay();
    updateFlushLabel();
    updateTotalValue();
    updateHeartsLabel();
    updateQueenOfSpadesLabel();
  }

  /**
   * Updates the display of the hand of cards.
   */
  private void updateHandDisplay() {
    StringBuilder handStr = new StringBuilder();
    for (PlayingCard card : hand.getCards()) {
      handStr.append(card.getAsString()).append(" ");
    }
    handDisplay.setText(handStr.toString());
  }

  /**
   * Updates the flush label.
   */
  private void updateFlushLabel() {
    if (hand.isFlush()) {
      flushLabel.setText("You have a flush!");
    } else {
      flushLabel.setText("You do not have a flush.");
    }
  }

  /**
   * Updates the total value label.
   */
  private void updateTotalValue() {
    int totalValue = hand.getTotalValue();
    totalValueLabel.setText("Total value: " + totalValue);
  }

  /**
   * Updates the hearts label.
   */
  public void updateHeartsLabel() {
    List<PlayingCard> hearts = hand.getCards().stream()
      .filter(card -> card.getSuit() == 'H')
      .collect(Collectors.toList());

    if (hearts.isEmpty()) {
      heartsLabel.setText("No hearts");
    } else {
      heartsLabel.setText("Hearts: " + hearts.stream()
        .map(PlayingCard::getAsString)
        .collect(Collectors.joining(", ")));
    }
  }

  /**
   * Updates the queen of spades label.
   */
  public void updateQueenOfSpadesLabel() {
    List<PlayingCard> queenOfSpades = hand.getCards().stream()
      .filter(card -> card.getSuit() == 'S' && card.getFace() == 12)
      .collect(Collectors.toList());

    if (queenOfSpades.isEmpty()) {
      queenOfSpadesLabel.setText("Queen of Spades: No");
    } else {
      queenOfSpadesLabel.setText("Queen of Spades: Yes");
    }
  }

  /**
   * The main entry point for this Java application.
   * This method is used to launch the JavaFX application.
   *
   * @param args Command-line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }

}

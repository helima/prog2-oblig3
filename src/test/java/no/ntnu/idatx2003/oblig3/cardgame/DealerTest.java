package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DealerTest {

  private Dealer dealerTest;

  @BeforeEach
  void setUp() {
    DeckOfCards deckOfCards = new DeckOfCards();
    dealerTest = new Dealer(deckOfCards);
  }

  @AfterEach
  void tearDown() {
  }

  @Test
  void testDealHand() {
    List<PlayingCard> hand = dealerTest.dealHand(5);
    assertEquals(5, hand.size(), "Expected hand to contain 5 cards");
  }

}
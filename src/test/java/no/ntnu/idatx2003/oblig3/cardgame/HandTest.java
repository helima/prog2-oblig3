package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HandTest {

  @BeforeEach
  void setUp() {
  }

  @AfterEach
  void tearDown() {
  }

  @Test
  void testAddCard() {
    Hand hand = new Hand();
    PlayingCard card = new PlayingCard('H', 2);
    hand.addCard(card);
    assertTrue(hand.getCards().contains(card), "Expected card to be added to hand");
    assertEquals('H', card.getSuit(), "Expected suit to be 'H', but got" + card.getSuit());
    assertEquals(2, card.getFace(), "Expected face to be 2, but got" + card.getFace());
  }

  @Test
  void testIsFlushTrue() {
    Hand hand = new Hand();
    hand.addCard(new PlayingCard('H', 2));
    hand.addCard(new PlayingCard('H', 3));
    hand.addCard(new PlayingCard('H', 4));
    hand.addCard(new PlayingCard('H', 5));
    hand.addCard(new PlayingCard('H', 6));
    assertTrue(hand.isFlush(), "Expected hand to be a flush");
  }

  @Test
  void testIsFlushFalse() {
    Hand hand = new Hand();
    hand.addCard(new PlayingCard('H', 2));
    hand.addCard(new PlayingCard('H', 3));
    hand.addCard(new PlayingCard('H', 4));
    hand.addCard(new PlayingCard('H', 5));
    hand.addCard(new PlayingCard('S', 6));
    assertFalse(hand.isFlush(), "Expected hand not to be a flush");
  }

}
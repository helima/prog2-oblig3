package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PlayingCardTest {

  @Test
  void testCreateValidCard() {
    PlayingCard card = new PlayingCard('H', 10);
    assertEquals('H', card.getSuit(), "Expected suit to be 'H'");
    assertEquals(10, card.getFace(), "Expected face to be 10");
  }

  @Test
  void testGetAsString() {
    PlayingCard card = new PlayingCard('H', 10);
    assertEquals("H10", card.getAsString(), "Expected string representation to be 'H10'");
  }

  @Test
  void testCreateInvalidCard() {
    assertThrows(IllegalArgumentException.class, () -> {
      new PlayingCard('X', 10);
    }, "Expected IllegalArgumentException for invalid suit");
    assertThrows(IllegalArgumentException.class, () -> {
      new PlayingCard('H', 15);
    }, "Expected IllegalArgumentException for invalid face");
  }

}
package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeckOfCardsTest {

  private DeckOfCards deckOfCardsTest;

  @BeforeEach
  void setUp() {
    deckOfCardsTest = new DeckOfCards();
  }


  @Test
  void testDeckBuilder() {
    assertEquals(52, deckOfCardsTest.deck.size(),
      "The deck should contain 52 cards");
  }


}